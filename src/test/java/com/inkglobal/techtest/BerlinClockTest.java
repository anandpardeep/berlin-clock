package com.inkglobal.techtest;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class BerlinClockTest {
	private BerlinClock clock;

	@BeforeClass
	public void setUp() {
		clock = new BerlinClock();
	}

	@DataProvider(name = "validTimes")
	public Object[][] getTimes() {
		return new String[][] { { "15:10:01", "O", "RRRO", "OOOO", "YYOOOOOOOOO", "OOOO" },
				{ "09:10:01", "O", "ROOO", "RRRR", "YYOOOOOOOOO", "OOOO" },
				{ "0:0:0", "Y", "OOOO", "OOOO", "OOOOOOOOOOO", "OOOO" },
				{ "23:59:59", "O", "RRRR", "RRRO", "YYRYYRYYRYY", "YYYY" },
				{ "05:35:20", "Y", "ROOO", "OOOO", "YYRYYRYOOOO", "OOOO" },
				{ "", "Y", "OOOO", "OOOO", "OOOOOOOOOOO", "OOOO" } };
	}

	@Test(dataProvider = "validTimes")
	public void testLights(String time, String expectedSecondsLine, String hrLine1, String hrLine2, String mtLine1,
			String mtLine2) {
		String lights[] = clock.convertTimeToVisualRepresentation(time);
		Assert.assertTrue(lights[0].equals(expectedSecondsLine));
		Assert.assertTrue(lights[1].equals(hrLine1));
		Assert.assertTrue(lights[2].equals(hrLine2));
		Assert.assertTrue(lights[3].equals(mtLine1));
		Assert.assertTrue(lights[4].equals(mtLine2));
	}
}
