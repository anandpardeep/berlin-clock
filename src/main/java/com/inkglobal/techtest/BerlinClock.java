package com.inkglobal.techtest;

public class BerlinClock {
	private final static String RED = "R";
	private final static String YELLOW = "Y";
	private final static String OFF = "O";
	private final static String MIDNIGHT_TIME = "00:00:00";

	public String[] convertTimeToVisualRepresentation(String time) {
		String[] timeComponent = null;
		if (null == time || "".equals(time.trim())) {
			timeComponent = MIDNIGHT_TIME.split(":");
		} else {
			timeComponent = time.split(":");
		}

		return new String[] { getSecondsLights(Integer.parseInt(timeComponent[2])),
				getTopRowLights(Integer.parseInt(timeComponent[0])),
				getSecondRowFromTopLights(Integer.parseInt(timeComponent[0])),
				getThirdRowFromTopLights(Integer.parseInt(timeComponent[1])),
				getFourthRowFromTopLights(Integer.parseInt(timeComponent[1])) };
	}

	private String getTopRowLights(int hour) {
		return toVisualString(hour / 5, 4, RED);
	}

	private String getSecondRowFromTopLights(int hour) {
		return toVisualString(hour % 5, 4, RED);
	}

	private String getThirdRowFromTopLights(int minute) {
		String minuteStr = toVisualString(minute / 5, 11, YELLOW);
		return minuteStr.replaceAll("YYY", "YYR");
	}

	private String getFourthRowFromTopLights(int minute) {
		return toVisualString(minute % 5, 4, YELLOW);
	}

	private String getSecondsLights(int second) {
		return second % 2 == 0 ? YELLOW : OFF;
	}

	private String toVisualString(int value, int numberOfLamps, String symbol) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < value; i++) {
			builder.append(symbol);
		}

		for (int i = value; i < numberOfLamps; i++) {
			builder.append(OFF);
		}

		return builder.toString();
	}
}
